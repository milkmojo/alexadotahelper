package org.alexapp.models.dotaclient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by lucaspedroza on 6/25/16.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class MatchDetailResponse {
    private String _matchId;
    private String _matchSeqNum;
    private String _startTime;
    private String _lobbyType;
    private String _radiantWin;
    private String _duration;
    private String _towerStatusRadiant;
    private String _towerStatusDire;
    private String _barracksStatusRadiant;
    private String _barracksStatusDire;
    private String _cluster;
    private String _firstBloodTime;
    private String _humanPlayers;
    private String _leagueId;
    private String _positiveVotes;
    private String _negativeVotes;
    private String _gameMode;
    private String _engine;
    private String _radiantCaptain;
    private String _direCaptain;
    private List<PickBan> _picksBans;
    private List<PlayerDetail> _playerDetails;

    @JsonProperty("match_id")
    public void setMatchId(String l) { this._matchId = l; }
    public String getMatchId() { return _matchId; }

    @JsonProperty("match_seq_num")
    public void setMatchSeqNum(String l) { this._matchSeqNum = l; }
    public String getMatchSeqNum() { return _matchSeqNum; }

    @JsonProperty("start_time")
    public void setStartTime(String l) { this._startTime = l; }
    public String getStartTime() { return _startTime; }

    @JsonProperty("lobby_type")
    public void setLobbyType(String b) { this._lobbyType = b; }
    public String getLobbyType() { return _lobbyType; }

    @JsonProperty("radiant_win")
    public void setRadiantWin(String i) { this._radiantWin = i; }
    public String getRadiantWin() { return _radiantWin; }

    @JsonProperty("duration")
    public void setDuration(String i) { this._duration = i; }
    public String getDuration() { return _duration; }

    @JsonProperty("tower_status_radiant")
    public void setTowerStatusRadiant(String i) { this._towerStatusRadiant = i; }
    public String getTowerStatusRadiant() { return _towerStatusRadiant; }

    @JsonProperty("tower_status_dire")
    public void setTowerStatusDire(String i) { this._towerStatusDire = i; }
    public String getTowerStatusDire() { return _towerStatusDire; }

    @JsonProperty("barracks_status_radiant")
    public void setBarracksStatusRadiant(String i) { this._barracksStatusRadiant = i; }
    public String getBarracksStatusRadiant() { return _barracksStatusRadiant; }

    @JsonProperty("barracks_status_dire")
    public void setBarracksStatusDire(String i) { this._barracksStatusDire = i; }
    public String getBarracksStatusDire() { return _barracksStatusDire; }

    @JsonProperty("cluster")
    public void setCluster(String i) { this._cluster = i; }
    public String getCluster() { return _cluster; }

    @JsonProperty("first_blood_time")
    public void setFirstBloodTime(String i) { this._firstBloodTime = i; }
    public String getFirstBloodTime() { return _firstBloodTime; }

    @JsonProperty("human_players")
    public void setHumanPlayers(String i) { this._humanPlayers = i; }
    public String getHumanPlayers() { return _humanPlayers; }

    @JsonProperty("leagueid")
    public void setLeagueId(String i) { this._leagueId = i; }
    public String getLeagueId() { return _leagueId; }

    @JsonProperty("positive_votes")
    public void setPositiveVotes(String i) { this._positiveVotes = i; }
    public String getPositiveVotes() { return _positiveVotes; }

    @JsonProperty("negative_votes")
    public void setNegativeVotes(String i) { this._negativeVotes = i; }
    public String getNegativeVotes() { return _negativeVotes; }

    @JsonProperty("game_mode")
    public void setGameMode(String i) { this._gameMode = i; }
    public String getGameMode() { return _gameMode; }

    @JsonProperty("engine")
    public void setEngine(String i) { this._engine = i; }
    public String getEngine() { return _engine; }

    @JsonProperty("radiant_captain")
    public void setRadiantCaptain(String s) { this._radiantCaptain = s; }
    public String getRadiantCaptain() { return _radiantCaptain; }

    @JsonProperty("dire_captain")
    public void setDireCaptain(String s) { this._direCaptain = s; }
    public String getDireCaptain() { return this._direCaptain; }

    @JsonProperty("picks_bans")
    public void setPicksBans(List<PickBan> pb) { this._picksBans = pb; }
    public List<PickBan> getPicksBans() { return _picksBans; }

    @JsonProperty("players")
    public void setPlayers(List<PlayerDetail> lp) { this._playerDetails = lp; }
    public List<PlayerDetail> getPlayers() { return _playerDetails; }

}


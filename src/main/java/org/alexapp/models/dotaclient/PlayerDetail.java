package org.alexapp.models.dotaclient;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by lucaspedroza on 6/25/16.
 */
public class PlayerDetail {
    private String _accountId;
    private String _playerSlot;
    private String _heroId;
    private String _item0;
    private String _item1;
    private String _item2;
    private String _item3;
    private String _item4;
    private String _item5;
    private String _kills;
    private String _deaths;
    private String _assists;
    private String _leaverStatus;
    private String _gold;
    private String _lastHits;
    private String _denies;
    private String _goldPerMinute;
    private String _xpPerMinute;
    private String _goldSpent;
    private String _heroDamage;
    private String _towerDamage;
    private String _heroHealing;
    private String _level;
    private List<AbilityUpgrade> _abilityUpgrades;
    private List<AdditionalUnit> _additionalUnits;

    @JsonProperty("account_id")
    public void setAccountId(String l) { _accountId = l; }
    public String getAccountId() { return _accountId; }

    @JsonProperty("player_slot")
    public void setPlayerSlot(String sh) { _playerSlot = sh; }
    public String getPlayerSlot() { return _playerSlot; }

    @JsonProperty("hero_id")
    public void setHeroId(String b) { _heroId = b; }
    public String getHeroId() { return _heroId; }

    @JsonProperty("item_0")
    public void setItem0(String b) { _item0 = b; }
    public String getItem0() { return _item0; }

    @JsonProperty("item_1")
    public void setItem1(String b) { _item1 = b; }
    public String getItem1() { return _item1; }

    @JsonProperty("item_2")
    public void setItem2(String b) { _item2 = b; }
    public String getItem2() { return _item2; }

    @JsonProperty("item_3")
    public void setItem3(String b) { _item3 = b; }
    public String getItem3() { return _item3; }

    @JsonProperty("item_4")
    public void setItem4(String b) { _item4 = b; }
    public String getItem4() { return _item4; }

    @JsonProperty("item_5")
    public void setItem5(String b) { _item5 = b; }
    public String getItem5() { return _item5; }

    @JsonProperty("kills")
    public void setKills(String b) { _kills = b; }
    public String getKills() { return _kills; }

    @JsonProperty("deaths")
    public void setDeaths(String b) { _deaths = b; }
    public String getDeaths() { return _deaths; }

    @JsonProperty("assists")
    public void setAssists(String b) { _assists = b; }
    public String getAssists() { return _assists; }

    @JsonProperty("leaver_status")
    public void setLeaverStatus(String b) { _leaverStatus = b; }
    public String getLeaverStatus() { return _leaverStatus; }

    @JsonProperty("gold")
    public void setGold(String b) { _gold = b; }
    public String getGold() { return _gold; }

    @JsonProperty("last_hits")
    public void setLastHits(String b) { _lastHits = b; }
    public String getLastHits() { return _lastHits; }

    @JsonProperty("denies")
    public void setDenies(String b) { _denies = b; }
    public String getDenies() { return _denies; }

    @JsonProperty("gold_per_min")
    public void setGoldPerMinute(String b) { _goldPerMinute = b; }
    public String getGoldPerMinute() { return _goldPerMinute; }

    @JsonProperty("xp_per_min")
    public void setXpPerMinute(String b) { _xpPerMinute = b; }
    public String getXpPerMinute() { return _xpPerMinute; }

    @JsonProperty("gold_spent")
    public void setGoldSpent(String b) { _goldSpent = b; }
    public String getGoldSpent() { return _goldSpent; }

    @JsonProperty("hero_damage")
    public void setHeroDamage(String b) { _heroDamage = b; }
    public String getHeroDamage() { return _heroDamage; }

    @JsonProperty("tower_damage")
    public void setTowerDamage(String b) { _towerDamage = b; }
    public String getTowerDamage() { return _towerDamage; }

    @JsonProperty("hero_healing")
    public void setHeroHealing(String b) { _heroHealing = b; }
    public String getHeroHealing() { return _heroHealing; }

    @JsonProperty("level")
    public void setLevel(String b) { _level = b; }
    public String getLevel() { return _level; }

    @JsonProperty("ability_upgrades")
    public void setAbilityUpgrades(List<AbilityUpgrade> al) { _abilityUpgrades = al; }
    public List<AbilityUpgrade> getAbilityUpgrades() { return _abilityUpgrades; }

    @JsonProperty("additional_units")
    public void setAdditionalUnits(List<AdditionalUnit> au) { _additionalUnits = au; }
    public List<AdditionalUnit> getAdditionalUnits() { return _additionalUnits; }

    public String[] getItems() {
        return new String[] {getItem0(), getItem1(), getItem2(),getItem3(),getItem4(),getItem5() };
    }
}

package org.alexapp.models.dotaclient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by lucaspedroza on 6/25/16.
 */
@JsonIgnoreProperties
public class Player {
    private String _accountId;
    private String _playerSlot;
    private String _heroId;

    @JsonProperty("account_id")
    public void setAccountId(String l) { _accountId = l; }
    public String getAccountId() { return _accountId; }

    @JsonProperty("player_slot")
    public void setPlayerSlot(String sh) { _playerSlot = sh; }
    public String getPlayerSlot() { return _playerSlot; }

    @JsonProperty("hero_id")
    public void setHeroId(String b) { _heroId = b; }
    public String getHeroId() { return _heroId; }

}

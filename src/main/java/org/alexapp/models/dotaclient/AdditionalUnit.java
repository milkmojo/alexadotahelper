package org.alexapp.models.dotaclient;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by lucaspedroza on 6/25/16.
 */
public class AdditionalUnit {
    private String _unitname;
    private String _item0;
    private String _item1;
    private String _item2;
    private String _item3;
    private String _item4;
    private String _item5;

    @JsonProperty("unitname")
    public void setUnitname(String s) { _unitname = s; }
    public String getUnitname() { return _unitname; }

    @JsonProperty("item_0")
    public void setItem0(String s) { _item0 = s; }
    public String getItem0() { return _item0; }

    @JsonProperty("item_1")
    public void setItem1(String s) { _item1 = s; }
    public String getItem1() { return _item1; }

    @JsonProperty("item_2")
    public void setItem2(String s) { _item2 = s; }
    public String getItem2() { return _item2; }

    @JsonProperty("item_3")
    public void setItem3(String s) { _item3 = s; }
    public String getItem3() { return _item3; }

    @JsonProperty("item_4")
    public void setItem4(String s) { _item4 = s; }
    public String getItem4() { return _item4; }

    @JsonProperty("item_5")
    public void setItem5(String s) { _item5 = s; }
    public String getItem5() { return _item5; }
}

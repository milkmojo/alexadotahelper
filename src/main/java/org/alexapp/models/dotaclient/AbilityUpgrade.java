package org.alexapp.models.dotaclient;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by lucaspedroza on 6/25/16.
 */
public class AbilityUpgrade {
    private String _ability;
    private String _time;
    private String _level;

    @JsonProperty("ability")
    public void setAbility(String l) { _ability = l; }
    public String getAbility() { return _ability; }

    @JsonProperty("time")
    public void setTime(String l) { _time = l; }
    public String getTime() { return _time; }

    @JsonProperty("level")
    public void setLevel(String l) { _level = l; }
    public String getLevel() { return _level; }

}
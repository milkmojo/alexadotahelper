package org.alexapp.models.dotaclient;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by lucaspedroza on 6/25/16.
 */
public class PickBan {
    private String _isPick;
    private String _heroId;
    private String _team;
    private String _order;

    @JsonProperty("is_pick")
    public void setIsPick(String s) { _isPick = s; }
    public String getIsPick() { return _isPick; }

    @JsonProperty("hero_id")
    public void setHeroId(String s) { _heroId = s; }
    public String getHeroId() { return _heroId; }

    @JsonProperty("team")
    public void setTeam(String s) { _team = s; }
    public String getTeam() { return _team; }

    @JsonProperty("order")
    public void setOrder(String s) { _order = s; }
    public String getOrder() { return _order; }

}


package org.alexapp.models.dotaclient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucaspedroza on 6/25/16.
 */
@JsonIgnoreProperties
public class DotaMatch {
    private String _matchId;
    private String _matchSeqNum;
    private String _startTime;
    private String _lobbyType;
    private String _radiantTeamId;
    private String _direTeamId;
    private List<Player> players = new ArrayList<Player>();

    @JsonProperty("match_id")
    public void setMatchId(String l) { _matchId = l; }
    public String getMatchId() { return _matchId; }

    @JsonProperty("match_seq_num")
    public void setMatchSeqNum(String l) { _matchSeqNum = l; }
    public String getMatchSeqNum() { return _matchSeqNum; }

    @JsonProperty("start_time")
    public void setStartTime(String l) { _startTime = l; }
    public String getStartTime() { return _startTime; }

    @JsonProperty("lobby_type")
    public void setLobbyType(String b) { _lobbyType = b; }
    public String getLobbyType() { return _lobbyType; }

    @JsonProperty("radiant_team_id")
    public void setRadiantTeamId(String i) { _radiantTeamId = i; }
    public String getRadiantTeamId() { return _radiantTeamId; }

    @JsonProperty("dire_team_id")
    public void setDireTeamId(String i) { _direTeamId = i; }
    public String getDireTeamId() { return _direTeamId; }

    @JsonProperty("players")
    public List<Player> getPlayers() { return players; }
    public void setPlayers(List<Player> players) { this.players = players; }
}
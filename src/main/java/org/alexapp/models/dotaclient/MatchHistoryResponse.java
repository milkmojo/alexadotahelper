package org.alexapp.models.dotaclient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by lucaspedroza on 6/25/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchHistoryResponse {
    private String _status;
    private List<DotaMatch> _dotaMatches;

    @JsonProperty("status")
    public void setStatus(String status) { this._status = status; }
    public String getStatus() { return _status; }

    @JsonProperty("matches")
    public void setMatches(List<DotaMatch> t) { this._dotaMatches = t; }
    public List<DotaMatch> getMatches() { return _dotaMatches; }


}
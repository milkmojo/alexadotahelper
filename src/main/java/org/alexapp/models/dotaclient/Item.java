package org.alexapp.models.dotaclient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by lucaspedroza on 6/25/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {
    private String _id;
    private String _name;
    private String _localizedName;

    @JsonProperty("id")
    public void setId(String id) { this._id = id; }
    public String getId() { return _id; }

    @JsonProperty("name")
    public void setName(String name) { this._name = name; }
    public String getName() { return _name; }

    @JsonProperty("localized_name")
    public void setLocalizedName(String localizedName) { this._localizedName = localizedName; }
    public String getLocalizedName() { return _localizedName; }

}

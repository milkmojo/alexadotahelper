package org.alexapp.models;

/**
 * Created by lucaspedroza on 6/12/16.
 */
public class SimpleMessage {
    public String user;
    public String message;

    public SimpleMessage(String user, String message) {
        this.user = user;
        this.message = message;
    }
}

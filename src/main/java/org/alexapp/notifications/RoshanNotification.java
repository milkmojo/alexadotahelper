package org.alexapp.notifications;

import org.alexapp.models.SimpleMessage;
import org.alexapp.Messenger;
/**
 * Created by lucaspedroza on 6/12/16.
 */
public class RoshanNotification implements Runnable {
    SimpleMessage message;

    public RoshanNotification(String user){
        message = new SimpleMessage(user,"Roshan might be up!");
    }

    public void run() {
        Messenger.deliver(message);
    }
}
package org.alexapp;

import org.alexapp.models.SimpleMessage;

public class Messenger {

    public static void deliver(SimpleMessage message) {
        System.out.println(String.format("User: %s, Message: %s", message.user, message.message));
    }

}

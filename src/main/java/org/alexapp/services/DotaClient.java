package org.alexapp.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.alexapp.models.dotaclient.MatchDetailResponse;
import org.alexapp.models.dotaclient.MatchHistoryResponse;
import org.alexapp.models.dotaclient.ResponseWrapper;


/**
 * Created by lucaspedroza on 6/25/16.
 */
public class DotaClient {
    private static final String BASEURL = "http://api.steampowered.com/IDOTA2Match_570/";
    private static final String APIKEY = "";
    private static final String MATCH_HISTORY_URL = "GetMatchHistory/v1";
    private static final String MATCH_DETAILS_URL = "GetMatchDetails/v1";
    //private static final String ACCOUNTID = "196243826";
    //GetMatchHistory/v1

    public MatchHistoryResponse getRecentMatchesByAccountAndHero(String accountId, String heroId) throws IOException {
        String url = BASEURL + MATCH_HISTORY_URL + String.format("?key=%s&account_id=%s&language=en&hero_id=%s",APIKEY,accountId,heroId);
        JsonParser parser = makeRequest(url);
        ObjectMapper mapper = new ObjectMapper();
        ResponseWrapper<MatchHistoryResponse> response = mapper.readValue(parser,new TypeReference<ResponseWrapper<MatchHistoryResponse>>() {});
        return response.result;
    }

    public MatchDetailResponse getSingleMatchDetailByMatchId(String matchId) throws IOException {
        String url = BASEURL + MATCH_DETAILS_URL + String.format("?key=%s&match_id=%s&language=en",APIKEY,matchId);
        JsonParser parser = makeRequest(url);
        ObjectMapper mapper = new ObjectMapper();
        ResponseWrapper<MatchDetailResponse> response = mapper.readValue(parser,new TypeReference<ResponseWrapper<MatchDetailResponse>>() {});
        return response.result;
    }

    private JsonParser makeRequest(String stringUrl) throws IOException {
        InputStreamReader inputStream = null;
        BufferedReader bufferedReader = null;
        StringBuilder builder = new StringBuilder();
        String line;
        URL url = new URL(stringUrl);
        JsonFactory factory = new JsonFactory();
        JsonParser parser = factory.createParser(url);
        return parser;
    }










}

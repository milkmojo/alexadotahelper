package org.alexapp.services;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.alexapp.models.dotaclient.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lucaspedroza on 6/25/16.
 */
public class DotaService {
    private static final HashMap<String,String> _itemMap;
    private static final HashMap<String,String> _heroMap;
    private static final DotaClient _client;

    static {
        _itemMap = cacheItems();
        _heroMap = cacheHeroes();
        _client = new DotaClient();
    }

    public static String[] getRecommendedItems(String accountId, String heroId) throws IOException, InterruptedException {
        if (heroId == null) {
            return null;
        }
        MatchHistoryResponse clientResponse = _client.getRecentMatchesByAccountAndHero(accountId,heroId);
        List<DotaMatch> matchList = clientResponse.getMatches();
        if (matchList.size() > 0) {
            for (int i = 0; i < matchList.size(); i++) {
                String matchId = matchList.get(i).getMatchId();
                MatchDetailResponse details = _client.getSingleMatchDetailByMatchId(matchId);
                if (!hasAbandon(details)) {
                    List<PlayerDetail> players = details.getPlayers();
                    for (int j = 0; j < players.size(); j++ ) {
                        PlayerDetail player = players.get(j);
                        if (player.getAccountId().equals(accountId)) {
                            return itemNumbersToNames(player.getItems());
                        }
                    }
                }
                Thread.sleep(250);
            }
        }
        return null;
    }

    private static boolean hasAbandon(MatchDetailResponse m) {
        List<PlayerDetail> players = m.getPlayers();
        for (int i = 0; i < players.size(); i++) {
            String leaver = players.get(i).getLeaverStatus();
            if(leaver.equals("0") || leaver.equals("1")) {
                continue;
            } else {
                return true;
            }
        }
        return false;
    }

    private static String[] itemNumbersToNames(String[] items) {
        String[] itemNames = new String[6];
        for (int i = 0; i < items.length; i++) {
            itemNames[i] = _itemMap.get(items[i]);
        }
        return itemNames;

    }

    public static HashMap<String,String> cacheItems(){
        try {
            InputStream stream = new FileInputStream("/Users/lucaspedroza/projects/alexaserver/items.json");
            JsonFactory factory = new JsonFactory();
            JsonParser parser = factory.createParser(stream);
            ObjectMapper mapper = new ObjectMapper();
            List<Item> items = mapper.readValue(parser,new TypeReference<ArrayList<Item>>() {});
            HashMap<String, String> itemMap = new HashMap<String,String>();
            for (int i = 0; i < items.size(); i++) {
                Item item = items.get(i);
                itemMap.put(item.getId(),item.getLocalizedName());
            }
            return itemMap;

        } catch (FileNotFoundException e) {
            System.err.println(String.format("Error opening item file: %s",e));
        } catch (IOException e) {
            System.err.println(String.format("Error serializing item file: %s",e));
        }
        return null;
    }

    public static HashMap<String,String> cacheHeroes(){
        try {
            InputStream stream = new FileInputStream("/Users/lucaspedroza/projects/alexaserver/heroes.json");
            JsonFactory factory = new JsonFactory();
            JsonParser parser = factory.createParser(stream);
            ObjectMapper mapper = new ObjectMapper();
            List<Hero> heroes = mapper.readValue(parser,new TypeReference<ArrayList<Hero>>() {});
            HashMap<String, String> heroMap = new HashMap<String,String>();
            for (int i = 0; i < heroes.size(); i++) {
                Hero item = heroes.get(i);
                heroMap.put(item.getLocalizedName().toLowerCase(),item.getId());
            }
            return heroMap;

        } catch (FileNotFoundException e) {
            System.err.println(String.format("Error opening item file: %s",e));
        } catch (IOException e) {
            System.err.println(String.format("Error serializing item file: %s",e));
        }
        return null;
    }
}

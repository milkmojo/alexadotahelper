package org.alexapp.controllers;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.alexapp.services.DotaService;

import java.io.*;
import java.util.*;

/**
 * Root resource (exposed at "assets" path)
 */
@Path("assets")
public class AssetRequests {

    @Path("heroes")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllHeroes () {
        try {
            File file = new File("/Users/lucaspedroza/projects/alexaserver/heroes.json");
            InputStream stream = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            stream.read(data);
            stream.close();
            System.out.println("Sending some hero data!");
            return new String(data,"UTF-8");
        } catch (FileNotFoundException e) {
            System.err.println(String.format("Error opening item file: %s",e));
        } catch (IOException e) {
            System.err.println(String.format("Error serializing item file: %s",e));
        }
        return null;
    }
}

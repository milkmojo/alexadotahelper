package org.alexapp.controllers;


import org.alexapp.RoshanNotificationScheduler;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

/**
 * Root resource (exposed at "roshan" path)
 */
@Path("roshan")
public class RoshanTimers {
    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    private static Map<String, RoshanNotificationScheduler> _roshanTimers;

    @Path("start")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public RoshanResponse startTimer (@QueryParam("user") String user) {
        RoshanResponse response = new RoshanResponse();
        if (_roshanTimers == null) {
            _roshanTimers = new HashMap<String, RoshanNotificationScheduler>();
        }
        if (!_roshanTimers.containsKey(user)){
            RoshanNotificationScheduler scheduler = new RoshanNotificationScheduler(user);
            scheduler.schedule();
            _roshanTimers.put(user, scheduler);
        } else if (_roshanTimers.get(user).getRemainingTime() < 0) {
            _roshanTimers.remove(user);
            RoshanNotificationScheduler scheduler = new RoshanNotificationScheduler(user);
            scheduler.schedule();
            _roshanTimers.put(user,scheduler);
        } else {
            response.success = false;
            response.message = "Roshan timer not started.";
            return response;
        }
        response.success = true;
        response.message = "New Roshan timer started.";
        response.timeRemaining = _roshanTimers.get(user).getRemainingTime();
        return response;
    }

    @Path("status")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public RoshanResponse checkTimer (@QueryParam("user") String user) {
        RoshanResponse response = new RoshanResponse();
        if (!_roshanTimers.containsKey(user)) {
            response.success = true;
            response.message = "No timer for this user.";
            response.timerActive = false;
            return response;
        } else {
            long timeLeft = _roshanTimers.get(user).getRemainingTime();
            response.success = true;
            response.message = String.format("Roshan should respawn in %d seconds", timeLeft);
            response.timeRemaining = timeLeft;
            response.timerActive = true;
            return response;
        }
    }

    private class RoshanResponse {
        public boolean success;
        public String message;
        public long timeRemaining;
        public boolean timerActive;
    }

}



package org.alexapp.controllers;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.alexapp.services.DotaService;

import java.io.IOException;

/**
 * Root resource (exposed at "roshan" path)
 */
@Path("recommend")
public class ItemRequests {
    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */

    @Path("items")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ItemResponse getItemRecommendationsByHeroAndAccount (@QueryParam("hero") String heroName) {
        System.out.println(String.format("Received a request for item recommendations for %s",heroName));
        ItemResponse response = new ItemResponse();
        try {
            String[] items = DotaService.getRecommendedItems("196243826", heroName);
            response.success = true;
            response.message = "Maybe I found some items!";
            response.items = items;
            return response;
        } catch (Exception ex) {
            System.err.println(String.format("Error getting recommended items: %s",ex));
        }
        response.success = false;
        response.message = "Error getting items";
        return response;
    }

    private class ItemResponse {
        public boolean success;
        public String message;
        public String[] items;
    }

}



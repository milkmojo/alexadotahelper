package org.alexapp;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ScheduledFuture;

import org.alexapp.notifications.RoshanNotification;

/**
 * Created by lucaspedroza on 6/12/16.
 */
public class RoshanNotificationScheduler {

    private static final int _minRoshanRespawnTimeInSeconds = 60; //TODO: should be 8 minutes
    private static int _timeSinceDeathInSeconds = 0;
    private final ScheduledExecutorService _scheduler = Executors.newScheduledThreadPool(1);
    private String user;
    private ScheduledFuture<?> roshanHandler;

    public RoshanNotificationScheduler(String user) {
        this.user = user;
    }

    public void schedule() {
        if (user.length() > 0) {
            RoshanNotification notification = new RoshanNotification(user);
            roshanHandler = _scheduler.schedule(notification, _minRoshanRespawnTimeInSeconds, TimeUnit.SECONDS);
        } else {
            System.err.println("No user received...");
        }
    }

    public long getRemainingTime() {
        return roshanHandler.getDelay(TimeUnit.SECONDS);
//        String messageText = String.format("Time remaining: %d", secondsRemaining);
//        SimpleMessage message = new SimpleMessage(user, messageText);
//        Messenger.deliver(message);
    }
}
